function Traveler (travelerName) {
    this.name = travelerName;
    this.food = 1;
    this.isHealthy = true;
 }
 function Wagon (wagonCapacity) {
     this.capacity = wagonCapacity
     this.passengers = []
 }
 Traveler.prototype = {
     constructor: Traveler,
     hunt: function(){
         this.food += 2;
     },
     eat: function(){
         if(this.food > 0){
             this.food -= 1;
         }else{
             this.isHealthy = false;
         }
     },
 }
 Wagon.prototype = {
     constructor: Traveler,
     getAvailableSeatCount: function(){
         let leftSpace = this.capacity - this.passengers.length
         return leftSpace;
     },

     join: function(traveler){
         if(this.getAvailableSeatCount() > 0){
             this.passengers.push(traveler)
         }
     },

     shouldQuarantine: function(){

        if(this.passengers.some(passenger => !passenger.isHealthy)) return true

        return false
     },

     totalFood: function(){
         foodCount = 0
         this.passengers.forEach(passenger => {
             foodCount += passenger.food
         });
         return foodCount
     }
 }
 // Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);